
### **Welcome to the Internet! [Or in this case, Orphic's Intranet!]**

Reference this page when you **need info or are updating the ORP-CLI or Tudio API**, like your organisation's [OpenAPI specifications](https://swagger.io/specification/), [Apigee guidelines](https://stackshare.io/stackups/apigee-vs-autocode-ide), or [gcloud assets](https://cloud.google.com/storage).

**Pre-requisite reading:**

1.  [Review Atlassian docs on writing tech docs](https://confluence.atlassian.com/doc/develop-technical-documentation-in-confluence-226166494.html)

2.  [Setup the Atlassian Plugin SDK and Dev Env](https://developer.atlassian.com/server/framework/atlassian-sdk/set-up-the-atlassian-plugin-sdk-and-build-a-project/)

3.  [Creating a Confluence Macro](https://developer.atlassian.com/server/confluence/creating-a-new-confluence-macro/)

**To start, you might want to:**

-   **Customise this overview **using the **edit icon** at the top right of this page.

-   **Create a new page **by clicking the **+** in the space sidebar.

-   [Contribute to the What Code Wiki](https://gitlab.com/orphic/what-code-wiki) with your personal profile/blog

**Tip**: Add the [label](https://confluence.atlassian.com/confcloud/use-labels-to-organize-your-content-724764874.html) *featured* to any pages you want to appear in the **Featured pages** list below.

Additional Technical Documentation:

[Excerpt Macro](https://confluence.atlassian.com/doc/excerpt-macro-148062.html)
