
# WHATCO.DE Wiki Home

This is [semi-live here](https://wiki.whatco.de/).

You've stumbled upon a page that requires commitment. Beyond intellect and wit, the criteria are just as hidden as what they reveal. Find yourself, and you will find the code hidden behind the screen. Beyond here is something like a utopia — beyond here is [WHATCO.DE](https://whatco.de).

[This is a mirage.](https://gitlab.whatco.de)

I've begun detailing the process for this project on [kyleo.io](https://kyleo.io/dreaming-in-code-lucid-dreaming/).

## [Privacy Policy](Privacy-Policy)


## Design Tenants

- Bleeding edge
—- If you’re going to break something do it on a branch/in integration first. 
- Web first.
- Break things fast (Test Driven Design)
—- Hurts and helps, especially with next point, and will likely rely on proprietary code. Golang/RUST > WebASM?
- KO believes the resolution to this will also fix email as a byproduct. (Fix email = receiving worthless email, spam, etc)
—- password reset, password successfully reset, new sign in attempt, 2FA email for passcode
—- yubikey-like hardware would be necessary and likely would need to be "Not Near Term" for awhile (blockchain = expensive/AI = ???)
- Education on underlying architectures as a byproduct of existence should be a testament to success, ie: increase in global user usage of using a `git` command (or deploy a site).


## Possible Todos
###### [GitLab Pages](https://docs.gitlab.com/ee/administration/pages/index.html)

This is officially blocked by an issue with GitLab: https://gitlab.com/gitlab-org/gitlab-pages/-/issues/137

###### [File Hooks](https://docs.gitlab.com/ee/administration/file_hooks.html)

## DEFINITE TODOS
Apple DTK home
https://developer.apple.com/programs/universal/

WWDC 2020 DTK related videos


[LINK TO THE APPLE DTK NDA](https://developer.apple.com/terms/universal-app-quick-start-program/Developer-Universal-App-Quick-Start-Program.pdf)

APIS to LIVE BY
====
Slack/Stripe

https://www.reddit.com/dev/api/

https://printfection.github.io/API-Documentation/

https://www.twilio.com/docs/usage/api

https://docs.netlify.com/api/get-started/

https://developer.bigcommerce.com/

https://medium.com/bigcommerce-developers

https://dev.twitch.tv/docs/api/

https://dev.twitch.tv/

https://developer.elgato.com/documentation/stream-deck/sdk/overview/

![whatco-de](uploads/9c4869a8f41580a5a2b532edf22cecb7/whatco-de.png)
What Code aims to be the apex between technology and art

[Serverless as a Service?](http://www.wuu.bi) Enable developers/entry-level devs to quickly spin up different docker images/SSGs.

Dashboard by Design? Similar to GitLab or Portainer, centralize information for these images.

What if this could fix billing? utilities/internet/phone because synced late/due dates would make this easier to track I think

Make this article but like this: https://blog.twitch.tv/en/2018/09/06/behind-the-development-the-pokemon-badge-collector-extension-af8ce9438187/?utm_referrer=https://dev.twitch.tv/showcase/
To be modeled after: https://www.gandi.net/en-US/no-bullshit